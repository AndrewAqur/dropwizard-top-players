package my.project.TopPlayers.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Андрюша on 11.07.2016.
 */
public class Player {
    private int id;
    private String nickname;
    private int score;

    public Player()
    {
        //for jackson
    }

    public Player(int id, String nickname, int score)
    {
        this.id=id;
        this.nickname=nickname;
        this.score=score;
    }
    @JsonProperty
    public int getId()
    {
        return id;
    }

    @JsonProperty
    public String getNickname()
    {
        return nickname;
    }

    @JsonProperty
    public int getScore()
    {
        return score;
    }
}
