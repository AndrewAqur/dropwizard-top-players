package my.project.TopPlayers.resources;

import com.codahale.metrics.annotation.Timed;
import my.project.TopPlayers.core.Player;
import my.project.TopPlayers.db.PlayerDB;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Андрюша on 11.07.2016.
 */
@Path("/player")
public class PlayerService {
    public PlayerService(){}

    @GET
    @Timed
    @Path("/get/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Player getPlayer (@PathParam("id") int id)
    {
        return PlayerDB.getById(id);
    }
    @GET
    @Timed
    @Path("/remove")
    @Produces(MediaType.TEXT_PLAIN)
    public String removePlayer() {
        PlayerDB.remove();
        return "Last player remove. Total count: " + PlayerDB.getCount();
    }

    @GET
    @Timed
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Player> getPlayers() {
        return PlayerDB.getAll();
    }

    @GET
    @Timed
    @Path("/allsorted")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Player> getPlayersSorted() {
        return PlayerDB.getTenSorted();
    }

    @POST
    @Timed
    @Path("/save")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes({MediaType.APPLICATION_JSON})
    public String addPlayer(Player player) {
        return PlayerDB.save(player);
    }
}
