package my.project.TopPlayers;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import my.project.TopPlayers.resources.PlayerService;

/**
 * Created by Андрюша on 11.07.2016.
 */
public class TopPlayersApplication extends Application<TopPlayersConfiguration> {
    public static void main(String[] args) throws Exception {
        new TopPlayersApplication().run(args);
    }
    @Override
    public void run(TopPlayersConfiguration config, Environment env)
    {
        final PlayerService playerService = new PlayerService();
        env.jersey().register(playerService);

    }
}
