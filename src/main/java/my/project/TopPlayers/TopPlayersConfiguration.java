package my.project.TopPlayers;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Андрюша on 11.07.2016.
 */
public class TopPlayersConfiguration extends Configuration {
    @NotEmpty
    private String version;

    @JsonProperty
    public String getVersion()
    {
        return version;
    }

    @JsonProperty
    public void setVersion(String version)
    {
        this.version=version;
    }
}
