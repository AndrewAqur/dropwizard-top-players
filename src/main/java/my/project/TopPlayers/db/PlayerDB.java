package my.project.TopPlayers.db;

import my.project.TopPlayers.core.Player;

import java.util.*;

/**
 * Created by Андрюша on 11.07.2016.
 */
public class PlayerDB {
    private static Map<Integer,Player> players = new HashMap<Integer, Player>();

    static
    {
        players.put(1, new Player(1,"Superman",39050));
        players.put(2, new Player(2,"Batman",54050));
        players.put(3, new Player(3,"Flash",100250));
        players.put(4, new Player(4,"MrPresident",12050));
        players.put(5, new Player(5,"Lex",20340));
        players.put(6, new Player(6,"John",9050));
        players.put(7, new Player(7,"Kevin",4050));
        players.put(8, new Player(8,"Lorem",150250));
        players.put(9, new Player(9,"Wes",1050));
        players.put(10, new Player(10,"Player",90340));

    }
    public static Player getById(int id)
    {
        return players.get(id);
    }
    public static List<Player> getAll()
    {
        List<Player> result = new ArrayList<Player>();
        for (Integer key :players.keySet())
        {
            result.add(players.get(key));
        }
        return result;
    }

    public static List<Player> getTenSorted()
    {
        List<Player> result = new ArrayList<Player>();
        for (Integer key :players.keySet())
        {
            result.add(players.get(key));
        }
        
        Collections.sort(result, new Comparator<Player>() {
            public int compare(Player o1, Player o2) {
                if (o1.getScore()>o2.getScore())
                    return -1;
                else if (o1.getScore()==o2.getScore())
                    return 0;
                else return 1;
            }
        });
//return Top Ten players
        return result.subList(0,10);
    }

    public static int getCount()
    {
        return players.size();
    }

    public static void remove()
    {
        if(!players.keySet().isEmpty())
        {
            players.remove(players.keySet().toArray()[0]);
        }
    }
    public static String save (Player player)
    {
        String result = "";
        if (players.get(player.getId())!=null)
        {
            result = "Updated Player with id=" +player.getId();
        }
        else
        {
            result = "Added Player with id=" +player.getId();
        }
        players.put(player.getId(),player);
        return result;
    }
}
